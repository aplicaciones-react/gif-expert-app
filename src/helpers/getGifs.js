export const getGifs = async (category) => {

    const url = `https://api.giphy.com/v1/gifs/search?q=${encodeURI(category)}&limit=10&api_key=fvzjEcG5iy9ZIyPo6gda0Y13erLq1H5N`;
    const resp = await fetch(url);
    const { data } = await resp.json();

    //Crear un nuevo objeto con los datos que necesitamos del api
    const gifs = data.map(img => {
        return {
            id: img.id,
            title: img.title,
            url: img.images?.downsized_medium.url
        }
    });

    return gifs;
}