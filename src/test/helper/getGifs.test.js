import React from 'react';
import { getGifs } from "../../helpers/getGifs";

describe('Pruebas con getGif fetch', () => {
    
    test('debe traer 10 elementos', async() => {
        const gifs = await getGifs('goku');
        expect(gifs.length).toBe(10)
    })
    
    test("debe traer 10 elementos", async () => {
        const gifs = await getGifs("");
        expect(gifs.length).toBe(0);
    });
})
