import React from 'react';
import '@testing-library/jest-dom';
import {shallow} from 'enzyme';
import { AddCategory } from '../../components/AddCategory';

describe('pruebas en el componente <AddCategory />/', () => {

    const setCategories = jest.fn();
    let wrapper = shallow(<AddCategory setCategories={setCategories} />);

    //Ciclo de vida de las pruebas
    beforeEach( () => {
        jest.clearAllMocks();
        wrapper = shallow(<AddCategory setCategories={setCategories} />);
    })
    
    test('debe de mostrarce correctamnte', () => {
        expect(wrapper).toMatchSnapshot();
    })

    test('debe de cambiar la caja de texto', () => {
        const input = wrapper.find('input');
        const value = "Hola Mundo";
        input.simulate('change', { 
            target: {
                value:value
            } 
        });

        expect(wrapper.find('p').text().trim()).toBe(value);
    })

    test('NO debe de postear la informacion con submit', () => {
        wrapper.find('form').simulate('submit', { preventDefault(){} }); //Se le envia el metodo prevent default ya que en la funcion lo necesita
        expect( setCategories ).not.toHaveBeenCalled();
    })
    
    test('Debe de llamar el setCategories y limpiar la caja de texto', () => {
        
        const value = "Hola Mundo";
        
        //Simular el input change
        wrapper.find('input').simulate('change', {target: {value: value}});

        //Simular el submit
        wrapper.find('form').simulate('submit', { preventDefault() {} });

        //setCategories debe ser llamado al menos una vez
        expect(setCategories).toHaveBeenCalled();
        expect(setCategories).toHaveBeenCalledTimes(1); //Se usa para validar la cantidad de veces que debe ser llamada una funcion
        expect(setCategories).toHaveBeenCalledWith( expect.any(Function) ); //Se usa para validar que los parametros enviados sean una funcion

        //El valor del input debe estar vacio
        expect(wrapper.find('input').prop('value')).toBe('');
    })
    

})
