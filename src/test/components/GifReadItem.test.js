import React from 'react';
import { shallow } from "enzyme";
import { GifReadItem } from '../../components/GifReadItem';

describe('Pruebas en <GifGridItem />', () => {

    const title = "Probando";
    const url   = "https://ssss/aaa.jpg";
    const wrapper = shallow(<GifReadItem title={title} url={url} />);
    
    test('debe mostrar el componente correctamente', () => {
        expect(wrapper).toMatchSnapshot();
    })

    test('debe de tener un parrafo con el title', () => {
        const p = wrapper.find('p');
        expect(p.text().trim()).toBe(title);
    })
    
    test("debe de tener la imagen igual al url y alt de los props", () => {
        const img = wrapper.find("img");
        //console.log(img.props())
        // Se valida la url de la iamgen
        expect(img.prop('src')).toBe(url);

        //Se valida el atributo alt de la iamgen
        expect(img.prop("alt")).toBe(title);
    });

    test("debe de tener animate__bounce", () => {
      const div = wrapper.find("div");

      const className = div.prop('className');

    //includes, busca dentro de un string
      expect(className.includes("animate__bounce")).toBe(true);
    });

})
