import React from 'react';
import '@testing-library/jest-dom';
import { shallow } from 'enzyme';
import { GifGrid } from '../../components/GifGrid';
import { useFetchGifs } from '../../hooks/useFetchGifs';
jest.mock('../../hooks/useFetchGifs')

describe('Pruebas en <GifGrid />', () => {
    
    const category = 'Goku';

    test('Debe mostrar el componente correctamente', () => {

        useFetchGifs.mockReturnValue({
            data : [],
            loading: true
        });

        const wrapper = shallow(<GifGrid category={category} />);
        expect(wrapper).toMatchSnapshot();
    })
    
    test('debe de mostrar items cuando se cargan imagenes con el useFetchGif', () => {
        
        const gifs = [{
            id: 'ABC',
            url: 'https://localhost/img.jpg',
            title: 'Cualquier cosa'
        }, {
            id: '123',
            url: 'https://localhost/img.jpg',
            title: 'Cualquier cosa'
        }];

        // para simular el consumo del api
        useFetchGifs.mockReturnValue({
            data: gifs,
            loading: false
        });

        const wrapper = shallow(<GifGrid category={category} />);

        //expect(wrapper).toMatchSnapshot();
        expect(wrapper.find('p').exists()).toBe(false);
        expect(wrapper.find('GifReadItem').length).toBe(gifs.length);
    })
    
})
