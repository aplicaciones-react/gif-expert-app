import React from 'react';
import { shallow } from 'enzyme';
import GifExpertApp from '../GifExpertApp';


describe('Pruebas al componente principal <GifExpertApp />', () => {
    
    test('Debe mostrar el compomente correctamente', () => {
        const wrapper = shallow(<GifExpertApp />);    
        expect(wrapper).toMatchSnapshot();    
    })
    
    test('debe de mostrar una lista de categorias', () => {
        
        const categories = ['One Punch', 'Dragon ball']
        const wrapper = shallow(<GifExpertApp defaultCategories={categories} />);   
         
        expect(wrapper).toMatchSnapshot();    
        expect(wrapper.find('GifGrid').length).toBe(categories.length);    
        
    })
    
})
