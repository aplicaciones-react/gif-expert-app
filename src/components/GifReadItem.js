import React from 'react';
import PropTypes from "prop-types";


export const GifReadItem = ( {title, url} ) => {

    return (
        <div className='card animate__animated animate__bounce'>
            <img src={url} alt={title}/>
            <p>{title}</p>
        </div>
    )
}

GifReadItem.propTypes = {
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};